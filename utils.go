package main

import (
	"fmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/gateway"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func initialize() (string, *gateway.Wallet) {
	wallet, err := gateway.NewFileSystemWallet(filepath.Join(getEnvOrDefault("WALLET_PATH",".."),"wallet"))
	if err != nil {
		log.Fatalf("Failed to create wallet: %v", err)
	}

	if !wallet.Exists("appUser") {
		err = populateWallet(wallet)
		if err != nil {
			log.Fatalf("Failed to populate wallet contents: %v", err)
		}
	}

	ccpPath := filepath.Join(getEnvOrDefault("CONN_PATH","/app/connection.yaml"))
	return ccpPath, wallet
}

func getEnvOrDefault(env, defaultVal string) string {
	value, ok := os.LookupEnv(env)
	if !ok {
		value = defaultVal
	}
	return value
}

func populateWallet(wallet *gateway.Wallet) error {

	log.Println("============ Populating wallet ============")
	credPath := filepath.Join(getEnvOrDefault("CRED_PATH","/users/Admin@dm.fabric.org/msp"))

	certPath := filepath.Join(credPath, "signcerts", "cert.pem")
	// read the certificate pem
	cert, err := ioutil.ReadFile(filepath.Clean(certPath))
	if err != nil {
		return err
	}

	keyDir := filepath.Join(credPath, "keystore")
	// there's a single file in this dir containing the private key
	files, err := ioutil.ReadDir(keyDir)
	if err != nil {
		return err
	}
	if len(files) != 1 {
		return fmt.Errorf("keystore folder should have contain one file")
	}
	keyPath := filepath.Join(keyDir, files[0].Name())
	key, err := ioutil.ReadFile(filepath.Clean(keyPath))
	if err != nil {
		return err
	}

	identity := gateway.NewX509Identity(getEnvOrDefault("CORE_PEER_LOCALMSPID","DMMSP"), string(cert), string(key))

	return wallet.Put("appUser", identity)
}
