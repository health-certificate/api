package main

import (
	"github.com/gin-gonic/gin"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/gateway"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/health-certificate/model"
	"log"
	"path/filepath"
	"strings"
)

func IssueCertificate(c *gin.Context) {
	var cert CertificateModel
	json := jsoniter.ConfigCompatibleWithStandardLibrary

	err := c.Bind(&cert)
	oc:= model.OpenCertificate{
		Id:                            cert.Id,
		IssueDate:                     cert.IssueDate,
		TypeCode:                      cert.TypeCode,
		LoadingPort:                   cert.LoadingPort.Name.Value,
		MainCarriageTransportMovement: cert.MainCarriageTransportMovement.UsedTransportMeans[0].Value,
		UnloadingPort:                 cert.UnloadingPort.Name.Value,
	}
	for _,item :=range cert.Items  {
		oc.Items = append(oc.Items, model.OpenFoodItem{
			Weight:           item.NetWeight.Measure,
			Quantity:         item.PhysicalPackage.Quantity,
			NumberOfPackages: 0,
			PackageSize:      item.NetWeight.UnitCode,
		})
	}
	ccpPath,wallet := initialize()
	gw, err := gateway.Connect(
		gateway.WithConfig(config.FromFile(filepath.Clean(ccpPath))),
		gateway.WithIdentity(wallet, "appUser"),
	)
	if err != nil {
		log.Fatalf("Failed to connect to gateway: %v", err)
	}
	defer gw.Close()
	network, err := gw.GetNetwork("irschannel")
	if err != nil {
		log.Fatalf("Failed to get network: %v", err)
	}
	contract := network.GetContract(getEnvOrDefault("CHAIN_CODE_ID","health_certificate"))

	log.Println("--> Submit Transaction: IssueCertificate, function create certificate on the ledger")
	transient := make(map[string][]byte)
	bytes, err := json.Marshal(cert)
	if err != nil {
		c.JSON(400,gin.H{
			"err":err.Error(),
		})
		return
	}
	transient["cert_data"] = bytes
	peers := strings.Split(getEnvOrDefault("END_PEERS","peer-1.dm.svc,peer-2.dm.svc,peer-3.dm.svc"),",")
	tx, err := contract.CreateTransaction("IssueCertificate",gateway.WithTransient(transient),gateway.WithEndorsingPeers(peers...))
	if err != nil {
		c.JSON(400,gin.H{
			"err":err.Error(),
		})
		return
	}
	payload:= model.Payload{Certificate: oc,Recipients: cert.Recipients}

	payloadJSON, _ := json.Marshal(payload)
	result, err := tx.Submit(string(payloadJSON))
	if err != nil {
		c.JSON(400,gin.H{
			"err":err.Error(),
		})
		return
	}
	log.Println(string(result))
	c.JSON(200, gin.H{
		"success": true,
	})
}

func ReadOpenCertificate(c *gin.Context){
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	cid := c.Param("id")
	ccpPath,wallet := initialize()
	gw, err := gateway.Connect(
		gateway.WithConfig(config.FromFile(filepath.Clean(ccpPath))),
		gateway.WithIdentity(wallet, "appUser"),
	)
	if err != nil {
		log.Printf("Failed to connect to gateway: %v", err)
		c.JSON(400,gin.H{
			"err":err.Error(),
		})
		return
	}
	defer gw.Close()

	network, err := gw.GetNetwork("irschannel")
	if err != nil {
		log.Printf("Failed to get network: %v", err)
		c.JSON(400,gin.H{
			"err":err.Error(),
		})
		return
	}

	contract := network.GetContract(getEnvOrDefault("CHAIN_CODE_ID","health_certificate"))
	peers := strings.Split(getEnvOrDefault("END_PEERS","peer-1.dm.svc,peer-2.dm.svc,peer-3.dm.svc"),",")
	tx,err := contract.CreateTransaction("ReadOpenCertificate",gateway.WithEndorsingPeers(peers...))
	if err != nil {
		log.Printf("Failed to Create Transaction: %v", err)
		c.JSON(400,gin.H{
			"err":err.Error(),
		})
		return
	}
	result, err := tx.Evaluate(cid)
	if err != nil {
		log.Printf("Failed to Evaluate Transaction: %v", err)
		c.JSON(400,gin.H{
			"err":err.Error(),
		})
		return
	}
	var payload model.Payload
	err = json.Unmarshal(result, &payload)
	if err != nil {
		log.Printf("Failed to unmarshal result: %v", err)
		c.JSON(400,gin.H{
			"err":err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"data": payload,
	})
}


func ReadCertificate(c *gin.Context){
	json := jsoniter.ConfigCompatibleWithStandardLibrary

	cid := c.Param("id")
	org :=getEnvOrDefault("CORE_PEER_LOCALMSPID","DMMSP")
	ccpPath,wallet := initialize()
	gw, err := gateway.Connect(
		gateway.WithConfig(config.FromFile(filepath.Clean(ccpPath))),
		gateway.WithIdentity(wallet, "appUser"),
	)
	if err != nil {
		log.Printf("Failed to connect to gateway: %v", err)
		c.JSON(400,gin.H{
			"err":err.Error(),
		})
		return
	}
	defer gw.Close()

	network, err := gw.GetNetwork("irschannel")
	if err != nil {
		log.Printf("Failed to get network: %v", err)
		c.JSON(400,gin.H{
			"err":err.Error(),
		})
		return
	}

	contract := network.GetContract(getEnvOrDefault("CHAIN_CODE_ID","health_certificate"))
	peers := strings.Split(getEnvOrDefault("END_PEERS","peer-1.dm.svc,peer-2.dm.svc,peer-3.dm.svc"),",")
	tx,err := contract.CreateTransaction("ReadCertificate",gateway.WithEndorsingPeers(peers...))
	if err != nil {
		log.Printf("Failed to Create Transaction: %v", err)
		c.JSON(400,gin.H{
			"err":err.Error(),
		})
		return
	}
	result, err := tx.Evaluate(cid,org)
	if err != nil {
		log.Printf("Failed to unmarshal result: %v", err)
		c.JSON(400,gin.H{
			"err":err.Error(),
		})
		return
	}
	var cert model.Certificate
	err = json.Unmarshal(result, &cert)
	c.JSON(200, gin.H{
		"data": cert,
	})
}