module gitlab.com/health-certificate/api

go 1.16

require (
	github.com/fvbock/endless v0.0.0-20170109170031-447134032cb6
	github.com/gin-gonic/gin v1.7.3
	github.com/hyperledger/fabric-sdk-go v1.0.0
	github.com/json-iterator/go v1.1.11
	gitlab.com/health-certificate/model v0.0.0-20210808082006-f8ba43dc0854
)
