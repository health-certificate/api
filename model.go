package main

import "gitlab.com/health-certificate/model"

type CertificateModel struct {
	model.Certificate
	Recipients []string `json:"recipients"`
}